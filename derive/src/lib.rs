#[macro_use]
extern crate darling;
extern crate syn;

use std::collections::HashSet;

use darling::ast::Data;
use darling::util::Override;
use darling::{FromDeriveInput, FromMeta};
use proc_macro2::TokenStream;
use proc_macro_error::{abort, proc_macro_error};
use quote::{quote, ToTokens};
use syn::punctuated::Punctuated;
use syn::spanned::Spanned;
use syn::{
    parse_quote, AngleBracketedGenericArguments, Expr, GenericArgument, Generics, Ident,
    PathArguments, Token, Type, TypePath, WhereClause, WherePredicate,
};

// #[derive(FormData)]
// struct LoginForm {
//     pub username: String,
//     #[field(validate = eq(self.username))]
//     pub password: String,
//     #[form_data(name = "Age")]
//     pub age: Option<u8>,
//     #[form_data(default = "token-12345")]
//     pub key: String,
// }

#[proc_macro_derive(FormData, attributes(field))]
#[proc_macro_error]
pub fn derive_router(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    match FormData::from_derive_input(&syn::parse_macro_input!(input)) {
        Ok(code) => quote! { #code }.into(),
        Err(err) => err.write_errors().into(),
    }
}

#[derive(Debug, Clone, FromDeriveInput)]
#[darling(supports(struct_any))]
struct FormData {
    ident: Ident,
    generics: Generics,
    // attrs: Vec<Attribute>,
    data: Data<(), Field>,
}

#[derive(Debug, Clone, FromField)]
#[darling(attributes(field))]
struct Field {
    ident: Option<Ident>,
    ty: Type,
    // attrs: Vec<Attribute>,

    // validate: Option<Expr>,
    name: Option<String>,
    default: Option<Override<DefaultValue>>,
}

#[derive(Clone, Debug)]
enum DefaultValue {
    Expr(Expr),
}

impl ToTokens for FormData {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let fields = match self.data.clone().take_struct() {
            Some(data) => data,
            None => abort!(self.ident, "FormData only support struct types"),
        };

        let name = &self.ident;
        let (impl_gen, type_gen, where_clause) = self.generics.split_for_impl();
        let fields_types: HashSet<&Type> = fields
            .iter()
            .filter_map(|f| (!is_string(&f.ty)).then(|| &f.ty))
            .map(get_field_type)
            .collect();
        let fields_idents: Punctuated<&Ident, Token![,]> =
            fields.iter().filter_map(|f| f.ident.as_ref()).collect();
        let parse_fields: TokenStream = fields.iter().map(Field::to_quote).collect();

        // add bounds `T: FromStr` for fields types
        let mut where_clause = where_clause.cloned().unwrap_or_else(|| WhereClause {
            where_token: Default::default(),
            predicates: Default::default(),
        });
        let where_for_types: Punctuated<WherePredicate, Token![,]> = fields_types
            .iter()
            .map(|ty| -> WherePredicate {
                parse_quote! { #ty: std::str::FromStr }
            })
            .collect();
        where_clause.predicates.extend(where_for_types);

        let gen_code = quote! {
            impl #impl_gen formalize::FormData for #name #type_gen
            #where_clause
            {
                type Error = anyhow::Error;

                fn from_raw(values: &std::collections::HashMap<String, String>) -> anyhow::Result<Self> {
                    #parse_fields

                    Ok(Self {
                        #fields_idents
                    })
                }
            }
        };

        tokens.extend(gen_code)
    }
}

impl Field {
    fn to_quote(&self) -> TokenStream {
        let ident = match self.ident.clone() {
            Some(ident) => ident,
            None => abort!(self.ty, "unnmaed field is not supported"),
        };
        let name = self.name.clone().unwrap_or_else(|| ident.to_string());
        let is_optional = is_optional(&self.ty);
        let use_from_str = !is_string(&self.ty);
        let has_default = self.default.is_some();

        let default_impl = match &self.default {
            None => quote! {},
            Some(Override::Inherit) => quote! { Default::default },
            Some(Override::Explicit(DefaultValue::Expr(expr))) => quote! { || { #expr }.into() },
        };

        if is_optional && has_default {
            let wrong_type = &self.ty;
            let wrong_type = quote! { #wrong_type }
                .to_string()
                .replace(char::is_whitespace, "");
            let correct_type = get_field_type(&self.ty);
            let correct_type = quote! { #correct_type }
                .to_string()
                .replace(char::is_whitespace, "");
            abort!(
                self.ty, "Cannot have optional field with default value";

                help = "use {} instead of {}", correct_type, wrong_type;
                note = "no need for {} type since you already set default value", wrong_type;
            )
        }

        let dynmic_part = match (is_optional, has_default, use_from_str) {
            (false, true, true) => quote! {
                .map(|val| {
                    match val.is_empty().then(#default_impl) {
                        Some(default) => Ok(default),
                        None => val.parse(),
                    }
                })
                .transpose()?
                .unwrap_or_else(#default_impl)
            },
            (false, false, true) => quote! {
                .ok_or(anyhow::anyhow!("missing {} field", #name))
                .and_then(|val| {
                    if val.is_empty() {
                        Err(anyhow::anyhow!("required field {} cannot be blank", #name))
                    } else {
                        Ok(val)
                    }
                })?
                .parse()?
            },
            (false, false, false) => quote! {
                .filter(|val| !val.is_empty())
                .ok_or(anyhow::anyhow!("missing {} field", #name))?
                .clone()
            },
            (false, true, false) => quote! {
                .cloned()
                .filter(|val| !val.is_empty())
                .unwrap_or_else(#default_impl)
            },
            (true, false, true) => quote! {
                .filter(|val| !val.is_empty())
                .map(|val| val.parse())
                .transpose()?
            },
            // These caeses doesn't happen beacuse we emit error when user have
            // optional value that have default value!
            //
            // (true, true, false) => quote! {
            //     .cloned()
            //     .unwrap_or_else(#default_impl)
            // },
            // (true, true, true) => quote! {
            //     .filter(|val| !val.is_empty())
            //     .map(|val| val.parse())
            //     .transpose()?
            //     .unwrap_or_else(#default_impl)
            // },
            _ => quote! {
                .cloned()
            },
        };

        let value = if is_optional && has_default {
            quote! { Some(values.get(#name)#dynmic_part) }
        } else {
            quote! { values.get(#name)#dynmic_part }
        };

        quote! { let #ident = #value; }
    }
}

impl FromMeta for DefaultValue {
    fn from_value(value: &syn::Lit) -> darling::Result<Self> {
        let expr_str = match value {
            syn::Lit::Str(lit_str) => lit_str,
            _ => {
                return Err(
                    darling::Error::custom("Value must be of literal string").with_span(&value)
                )
            }
        };

        let expr: Expr = expr_str.parse()?;

        Ok(DefaultValue::Expr(expr))
    }
}

fn is_optional(ty: &Type) -> bool {
    matches!(
        ty, Type::Path(TypePath { path, .. })
            if path.segments.last().map(|s| s.ident.to_string()) == Some("Option".to_string())
    )
}

fn is_string(ty: &Type) -> bool {
    matches!(
        ty, Type::Path(TypePath { path, .. })
            if path.get_ident() == Some(&Ident::new("String", ty.span()))
    )
}

fn get_field_type(ty: &Type) -> &Type {
    if let Type::Path(TypePath { path, .. }) = ty {
        if let Some(last) = path.segments.last() {
            if last.ident == "Option" {
                if let PathArguments::AngleBracketed(AngleBracketedGenericArguments {
                    args, ..
                }) = &last.arguments
                {
                    if let [GenericArgument::Type(inner_ty)] =
                        args.iter().collect::<Vec<_>>().as_slice()
                    {
                        return inner_ty;
                    }
                }
            }
        }
    }

    ty
}
