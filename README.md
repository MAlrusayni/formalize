# Formalize

[![master docs](https://img.shields.io/badge/docs-master-blue.svg)](https://malrusayni.gitlab.io/formalize/formalize/)
&middot;
[![stable docs](https://img.shields.io/badge/docs-stable-blue.svg)](https://docs.rs/formalize)
&middot;
[![crate info](https://img.shields.io/crates/v/formalize.svg)](https://crates.io/crates/formalize)
&middot;
[![pipeline](https://gitlab.com/MAlrusayni/formalize/badges/master/pipeline.svg)](https://gitlab.com/MAlrusayni/formalize/pipelines)
&middot;
[![rustc version](https://img.shields.io/badge/rustc-stable-green.svg)](https://crates.io/crates/formalize)
&middot;
[![unsafe forbidden](https://img.shields.io/badge/unsafe-forbidden-success.svg)](https://github.com/rust-secure-code/safety-dance/)

This crate provides you a **typed Form and Input** components. [See docs (master)](https://malrusayni.gitlab.io/formalize/formalize/)

**NOTE:** formalize is not (yet) production ready but is good for use in side projects and internal tools.

# Features

- **Typed Components**: Ensures you get the right data type not a raw string.
- **Validation**: All data are validated with traits like `FromStr` and `FromFormData`.
- **Errors**: All components support reporting errors in ergonomic way.
- **Replaceable Design**: Components by default designed using *TailwindCSS*, but you can replace it with your own design to match your design system.

# Qucik Example

```rust,ignore
use dioxus::prelude::*;
use formalize::{form, input, Form, FormData, Input};
use gloo::console::log;

fn main() {
    dioxus::web::launch(app);
}

fn app(cx: Scope) -> Element {
    let result = use_state(&cx, String::default);
    cx.render(rsx! {
        div {
            class: "flex flex-col gap-2 p-2",

            Form::<form::Props<LoginForm>> {
                class: "flex flex-col gap-2",
                on_valid: move |login_form| result.set(format!("{login_form:?}")),
                on_invalid: move |error| result.set(format!("{error:?}")),

                Input::<input::Props<u128>> {
                    label: "Id",
                    name: "id",
                    required: true,
                }
                Input::<input::Props<String>> {
                    label: "First name",
                    name: "firstname",
                    required: true,
                    state: use_state(&cx, || "Ali".into())
                }
                Input::<input::Props<String>> {
                    label: "Last name",
                    name: "lastname",
                    required: true,
                }
                Input::<input::Props<String>> {
                    label: "Phone",
                    name: "phone",
                }
                Input::<input::Props<String>> {
                    label: "Address",
                    name: "address",
                }
                Input::<input::Props<u8>> {
                    label: "Age",
                    name: "age",
                }
                Input::<input::Props<u8>> {
                    label: "Length",
                    name: "length",
                    required: true,
                }
                Input::<input::Props<u8>> {
                    label: "Weight",
                    name: "weight",
                }

                button {
                    class: "rounded shadow-sm bg-sky-500 text-white border-none py-2 px-4",
                    "type": "submit",
                    onclick: move |_| log!("clicked"),

                    "Login"
                }
            }
            div {
                class: "bg-sky-100 text-sky-500 p-4 rounded",

                "result: {result}"
            }
        }
    })
}

#[allow(dead_code)]
#[derive(Debug, FormData)]
struct LoginForm {
    pub id: u128,
    #[field(name = "firstname")]
    pub name: String,
    pub lastname: String,
    #[field(default = "\"05\"")]
    pub phone: String,
    pub address: Option<String>,
    #[field(default = "24")]
    pub age: u8,
    pub length: u8,
    pub weight: Option<i32>,
}
```

And this is how it looks:

![Screenshot](screenshots/demo.gif)
