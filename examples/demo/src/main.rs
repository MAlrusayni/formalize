use dioxus::prelude::*;
use formalize::{Form, FormData, Input};
use gloo::console::log;

fn main() {
    dioxus_web::launch(app)
}

fn app(cx: Scope) -> Element {
    let result = use_state(&cx, String::default);
    cx.render(rsx! {
        div {
            class: "flex flex-col gap-2 p-2",

            Form::<LoginForm> {
                class: "flex flex-col gap-2",
                on_valid: move |login_form| result.set(format!("{login_form:?}")),
                on_invalid: move |error| result.set(format!("{error:?}")),

                Input::<u128> {
                    label: "Id",
                    name: "id",
                    required: true,
                }
                Input::<String> {
                    label: "First name",
                    name: "firstname",
                    required: true,
                    state: use_state(&cx, || "Ali".into())
                }
                Input::<String> {
                    label: "Last name",
                    name: "lastname",
                    required: true,
                }
                Input::<String> {
                    label: "Phone",
                    name: "phone",
                }
                Input::<String> {
                    label: "Address",
                    name: "address",
                }
                Input::<u8> {
                    label: "Age",
                    name: "age",
                }
                Input::<u8> {
                    label: "Length",
                    name: "length",
                    required: true,
                }
                Input::<u8> {
                    label: "Weight",
                    name: "weight",
                }

                button {
                    class: "rounded shadow-sm bg-sky-500 text-white border-none py-2 px-4",
                    "type": "submit",
                    onclick: move |_| log!("clicked"),

                    "Login"
                }
            }
            div {
                class: "bg-sky-100 text-sky-500 p-4 rounded",

                "result: {result}"
            }
        }
    })
}

#[allow(dead_code)]
#[derive(Debug, FormData)]
struct LoginForm {
    pub id: u128,
    #[field(name = "firstname")]
    pub name: String,
    pub lastname: String,
    #[field(default = "\"05\"")]
    pub phone: String,
    pub address: Option<String>,
    #[field(default = "24")]
    pub age: u8,
    pub length: u8,
    pub weight: Option<i32>,
}
