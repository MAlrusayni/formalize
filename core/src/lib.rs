#![doc = include_str!("../../README.md")]
#![forbid(unsafe_code)]

pub use form::{Form, FormData};
pub use formalize_derive::FormData;
pub use input::Input;

pub mod form;
pub mod input;
