use core::fmt;
use std::collections::HashMap;

use dioxus::{events::FormEvent, prelude::*};

#[derive(Props)]
pub struct Props<'a, T: FormData + 'a> {
    /// Optional class used for styling the form
    #[props(into, optional)]
    class: Option<&'a str>,
    /// Closure to call while submitting the form, this will be called only when the form able to
    /// construct `T` out of the form values.
    ///
    /// It takes `T` value
    #[props(into, optional)]
    on_valid: Option<EventHandler<'a, T>>,
    /// Closure to call while submitting the form, this will be called only when the form failed
    /// to construct `T` out of the form values
    ///
    /// It takes the error that is returned from constructs method `T::from_form_data` so you can
    /// handle it your self.
    #[props(into, optional)]
    on_invalid: Option<EventHandler<'a, <T as FormData>::Error>>,
    children: Element<'a>,
}

#[allow(non_snake_case)]
pub fn Form<'a, T>(cx: Scope<'a, Props<'a, T>>) -> Element
where
    T: FormData,
{
    let class = cx.props.class.unwrap_or("");
    cx.render(rsx!(form {
        class: "{class}",
        prevent_default: "onsubmit",
        method: "post",
        onsubmit: move |data: FormEvent| {
            match T::from_raw(&data.values) {
                Ok(data) => {
                    if let Some(on_valid) = cx.props.on_valid.as_ref() {
                        on_valid.call(data)
                    }
                }
                Err(err) => {
                    if let Some(on_invalid) = cx.props.on_invalid.as_ref() {
                        on_invalid.call(err)
                    }
                }
            }
        },

        cx.props.children.as_ref()
    }))
}

/// A trait to construct types out of form data.
///
/// Any type implement this trait can be used with `From` component. You would
/// get value of this type on valid form data.
///
/// # Derive
///
/// This trait is derivable, you only need to use `#[derive(FromData)]`
/// and your type would be ready to be used with `Form` component.
///
/// Note that:
/// - Any type that is wraped in `Option` considered optional field.
/// - the resut considered required field.
///
/// By default field name will be used get its value from the form data, you can
/// pick diffrent name using `#[field(name = "new_name")]`.
///
/// Required field can have default value using `#[field(default)]` or
/// `#[field(default = "expr")]`, if the form data doesn't have this field, the
/// default value will be used instead.
///
/// Optional field cannot have default value, complile-time error will be thrown
/// if any optional field do.
///
/// All fields values will be constructed using `FromStr`, except `String`. Any
/// field type doesn't do, a complile-time error will be thrown.
///
/// ## Example
///
/// ```rust
/// use formalize::{FormData};
///
/// #[derive(Debug, FormData)]
/// struct LoginForm {
///     pub id: u128,
///     #[field(name = "firstname")]
///     pub name: String,
///     pub lastname: String,
///     #[field(default = "\"05\"")]
///     pub phone: String,
///     pub address: Option<String>,
///     #[field(default = "24")]
///     pub age: u8,
///     pub length: u8,
///     pub weight: Option<i32>,
/// }
/// ```
pub trait FormData: Sized {
    type Error: fmt::Debug;

    fn from_raw(values: &HashMap<String, String>) -> Result<Self, Self::Error>;
}
