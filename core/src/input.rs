use std::{fmt::Display, rc::Rc, str::FromStr};

use dioxus::{events::FormEvent, prelude::*};
use typed_builder::TypedBuilder;

#[derive(Props, Clone)]
pub struct Props<'a, T: 'static> {
    /// Label used for the component
    #[props(into, optional)]
    label: Option<&'a str>,

    /// Input name used by forms when submitting the form
    #[props(into)]
    name: &'a str,

    /// Optional state for component if you want to control the state from the parent.
    #[props(optional)]
    state: Option<&'a UseState<T>>,

    /// Make sure input will always have valid state no matter what.
    ///
    /// Note: this functionality requires you to pass a state to work.
    ///
    /// default: false
    #[props(default = false)]
    keep_last_valid_value: bool,

    /// disable the component
    ///
    /// default: false
    #[props(default = false)]
    disabled: bool,

    /// Mark the input as required
    ///
    /// default: false
    #[props(default = false)]
    required: bool,
}

#[allow(non_snake_case)]
pub fn Input<'a, T>(cx: Scope<'a, Props<'a, T>>) -> Element
where
    T: FromStr + Display + Clone,
    <T as FromStr>::Err: Display,
{
    // error value, if any
    let error = use_state(&cx, || None);
    let raw_value = use_state(&cx, move || None);
    let Props {
        label,
        name,
        state,
        keep_last_valid_value,
        disabled,
        required,
    } = cx.props;

    let classes = match use_context::<InputStyle>(&cx) {
        Some(user_styler) => user_styler.style(*disabled, error.get().is_some()),
        None => InputStyle::default().style(*disabled, error.get().is_some()),
    };

    if state.is_some() && raw_value.is_none() {
        raw_value.set(state.map(|s| s.get().to_string()))
    }

    let set_state = |val| {
        if let Some(state) = cx.props.state {
            state.set(val)
        }
    };

    let on_input = move |event: FormEvent| {
        let is_empty = event.value.is_empty();

        raw_value.set(Some(event.value.clone()));
        if is_empty && *required {
            error.set(Some("This field is required".to_string()));
            return;
        }

        if is_empty && !required {
            raw_value.set(Some(event.value.clone()));
            error.set(None);
            return;
        }

        match T::from_str(&event.value) {
            Ok(val) => {
                set_state(val);
                error.set(None);
            }
            Err(err) => error.set(Some(err.to_string())),
        }
    };

    let on_change = move |event: FormEvent| {
        let is_empty = event.value.is_empty();
        if is_empty && !required {
            raw_value.set(Some("".to_string()));
            error.set(None);
            return;
        }

        match T::from_str(&event.value) {
            Ok(val) => {
                raw_value.set(Some(event.value.clone()));
                set_state(val);
                error.set(None);
            }
            Err(_err) if *keep_last_valid_value => {
                if let Some(state) = state {
                    let value = state.get().to_string();
                    raw_value.set(Some(value));
                    error.set(None)
                }
            }
            Err(err) => error.set(Some(err.to_string())),
        }
    };

    let input = rsx! {
        input {
            name: "{name}",
            class: "{classes.input_element}",
            value: format_args!("{}", raw_value.get().clone().unwrap_or_default()),
            oninput: on_input,
            onchange: on_change,
            disabled: "{disabled}",
            required: "{required}",
        }
    };

    cx.render(rsx!(
        div {
            class: "{classes.container}",

            match label.as_ref() {
                None => input,
                Some(text) => rsx!{ label {
                  class: "{classes.label}",

                  required
                    .then(|| rsx!{ "{text} *" })
                    .unwrap_or_else(|| rsx!{ "{text}" })

                  input
              }}
            }

            error.get().as_ref().map(|err| {
                rsx!(p {
                    class: "{classes.error}",
                    "{err}"
                })
            })
        }
    ))
}

#[derive(TypedBuilder)]
pub struct Classes {
    #[builder(setter(into))]
    container: String,
    #[builder(setter(into))]
    label: String,
    #[builder(setter(into))]
    input_element: String,
    #[builder(setter(into))]
    error: String,
}

#[derive(Clone)]
pub struct InputStyle(Rc<dyn InputStyleImpl>);

impl Default for InputStyle {
    fn default() -> Self {
        InputStyle::new(DefaultInputStyle)
    }
}

pub trait InputStyleImpl {
    fn style(&self, disabled: bool, error: bool) -> Classes;
}

impl InputStyle {
    pub fn new(style: impl InputStyleImpl + 'static) -> Self {
        Self(Rc::new(style))
    }

    pub fn style(&self, disabled: bool, error: bool) -> Classes {
        self.0.style(disabled, error)
    }
}

pub struct DefaultInputStyle;

impl InputStyleImpl for DefaultInputStyle {
    fn style(&self, _disabled: bool, error: bool) -> Classes {
        let invalid: &'static str = match error {
            true => "border-pink-500 text-pink-600 focus:border-pink-500 focus:text-pink-600 focus:ring-pink-600 hover:border-pink-500",
            false => "",
        };

        Classes::builder()
            .container("flex flex-col gap-2")
            .label("flex flex-col gap-1 text-sm font-medium")
            .input_element(format!(
                "border rounded px-3 py-2 w-full outline-none shadow-sm placeholder-slate-400 \
                 disabled:bg-slate-50 disabled:text-slate-500 disabled:border-slate-200 disabled:shadow-none \
                 focus:outline-none focus:border-sky-500 focus:ring-1 focus:ring-sky-500 \
                 hover:border-sky-300 \
                 invalid:border-pink-500 invalid:text-pink-600 \
                 focus:invalid:border-pink-500 focus:invalid:ring-pink-500 \
                 hover:invalid:border-pink-500 hover:invalid:ring-pink-500 \
                 {invalid}",
            ))
            .error("bg-red-50 text-pink-600 border rounded border-pink-500 p-2")
            .build()
    }
}
